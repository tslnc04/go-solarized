# go-solarized

Using [go-colors](https://gitlab.com/tslnc04/go-colors) to provide easy access to solarized colours.

## Usage

There are variables for each of the solarized colours. Simple add to
a string to get the effects. In addition, there are variables for
resetting the colour, clearing the screen, and clearing the line. Some functions
are also provided, notably `Random` or `Rc`, which provide a random colour.
`Multi` is also provided for making each character of a string a different colour
than the next.
