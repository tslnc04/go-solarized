package main

import (
	"fmt"
	"gitlab.com/tslnc04/go-solarized"
)

func main() {
	fmt.Println(solarized.Multi("Hello, world!"))
}
