package solarized

import (
	"gitlab.com/tslnc04/go-colors"
	"math/rand"
	"time"
)

var (
	Base03  = colors.CombineEffects(colors.FourBitColor(8, true))
	B03     = Base03
	Base02  = colors.CombineEffects(colors.FourBitColor(0, true))
	B02     = Base02
	Base01  = colors.CombineEffects(colors.FourBitColor(10, true))
	B01     = Base01
	Base00  = colors.CombineEffects(colors.FourBitColor(11, true))
	B00     = Base00
	Base3   = colors.CombineEffects(colors.FourBitColor(15, true))
	B3      = Base03
	Base2   = colors.CombineEffects(colors.FourBitColor(7, true))
	B2      = Base02
	Base1   = colors.CombineEffects(colors.FourBitColor(14, true))
	B1      = Base01
	Base0   = colors.CombineEffects(colors.FourBitColor(12, true))
	B0      = Base0
	Yellow  = colors.CombineEffects(colors.FourBitColor(3, true))
	Y       = Yellow
	Orange  = colors.CombineEffects(colors.FourBitColor(9, true))
	O       = Orange
	Red     = colors.CombineEffects(colors.FourBitColor(1, true))
	R       = Red
	Magenta = colors.CombineEffects(colors.FourBitColor(5, true))
	M       = Magenta
	Violet  = colors.CombineEffects(colors.FourBitColor(13, true))
	V       = Violet
	Blue    = colors.CombineEffects(colors.FourBitColor(4, true))
	B       = Blue
	Cyan    = colors.CombineEffects(colors.FourBitColor(6, true))
	C       = Cyan
	Green   = colors.CombineEffects(colors.FourBitColor(2, true))
	G       = Green
	Reset   = colors.ClearEffects
	X       = Reset
	Clear   = colors.ClearScreen
	Cl      = Clear
	Line    = colors.ClearLine
	L       = Line
)

var Colors = []string{Y, O, R, M, V, B, C, G}

func Random() string {
	rand.Seed(time.Now().UnixNano())
	return Colors[rand.Intn(len(Colors))]
}

func Rc() string {
	return Random()
}

func Multi(text string) string {
	var output string

	for _, ch := range text {
		output += Rc() + string(ch)
	}

	output += X

	return output
}
